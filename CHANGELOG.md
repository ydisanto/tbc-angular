## [3.0.1](https://gitlab.com/to-be-continuous/angular/compare/3.0.0...3.0.1) (2022-08-08)


### Bug Fixes

* **e2e:** skip if enabled var unset ([13eec55](https://gitlab.com/to-be-continuous/angular/commit/13eec5524043d7d4db7bd319f867660cc3ccea23))

# [3.0.0](https://gitlab.com/to-be-continuous/angular/compare/2.2.0...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([6bbe6cf](https://gitlab.com/to-be-continuous/angular/commit/6bbe6cfccd689f51149674bb63789c48ac2800c6))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.2.0](https://gitlab.com/to-be-continuous/angular/compare/2.1.0...2.2.0) (2022-05-01)


### Features

* configurable tracking image ([82f6bf1](https://gitlab.com/to-be-continuous/angular/commit/82f6bf10ab7185dfe6680935a44f53a6d572e96b))

# [2.1.0](https://gitlab.com/to-be-continuous/angular/compare/2.0.1...2.1.0) (2021-12-02)


### Features

* add support for npm scoped registries ([8522ee3](https://gitlab.com/to-be-continuous/angular/commit/8522ee3e3c2939c53284c07115d9804ec79bc9ab))

## [2.0.1](https://gitlab.com/to-be-continuous/angular/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([1b35ffe](https://gitlab.com/to-be-continuous/angular/commit/1b35ffef89268f7c672f9dcea2e6b6b2ae8a9040))

## [2.0.0](https://gitlab.com/to-be-continuous/angular/compare/1.2.0...2.0.0) (2021-09-02)

### Features

* Change boolean variable behaviour ([6b573ea](https://gitlab.com/to-be-continuous/angular/commit/6b573ea47079cd20f7c7765409a8e76d1b5b1247))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.0](https://gitlab.com/to-be-continuous/angular/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([0541826](https://gitlab.com/to-be-continuous/angular/commit/054182647fa378014daeefd81c4f7fb2e433ce02))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/angular/compare/1.0.1...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([11ba7ae](https://gitlab.com/Orange-OpenSource/tbc/angular/commit/11ba7ae7b3eb5869f737a3a1b5bcb9220749bb39))

## [1.0.1](https://gitlab.com/Orange-OpenSource/tbc/angular/compare/1.0.0...1.0.1) (2021-05-12)

### Bug Fixes

* remove double $NG_WORKSPACE_DIR in JUnit report ([960fe3d](https://gitlab.com/Orange-OpenSource/tbc/angular/commit/960fe3dba7633c1a482adc5296a70465197c24db))

## 1.0.0 (2021-05-06)

### Features

* initial release ([6645031](https://gitlab.com/Orange-OpenSource/tbc/angular/commit/6645031f9a66dee712976a42f9b9e53e278fb8ad))
